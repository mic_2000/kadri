class Skill < ActiveRecord::Base
  attr_accessible :name

  has_many :resumes, :through => :skillings, :source => :skillable, :source_type => "Resume"
  has_many :vacancies, :through => :skillings, :source => :skillable, :source_type => "Vacancy"
  has_many :skillings

  def self.valid_skills
    skill_ids = Skilling.all.map(&:skill_id).uniq
    Skill.find skill_ids
  end
end
