class Vacancy < ActiveRecord::Base
  require 'common_behavior'
  include CommonBehavior
  attr_accessible :email, :phone, :duedate, :name, :salary, :skill_list

  scope :active, where('duedate >= ?', Date.today).order('salary desc')

  def fully_matched_vacancies
    self.fully_matched.active
  end

  def partially_matched_vacancies
    self.partially_matched.active
  end
end
