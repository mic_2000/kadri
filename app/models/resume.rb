# encoding: UTF-8
class Resume < ActiveRecord::Base
  require 'common_behavior'
  include CommonBehavior
  attr_accessible :email, :phone, :name, :salary, :status, :skill_list

  validates_format_of :name, :with => /\A[а-яА-ЯёЁ]+\s+[а-яА-ЯёЁ]+\s+[а-яА-ЯёЁ]+$\Z/i, message: "Фамилия Имя Отчество, только русские буквы"

  scope :active, where(status: true).order('salary asc')

  def fully_matched_resumes
    self.fully_matched.active
  end

  def partially_matched_resumes

    self.partially_matched.active
  end
end
