class SkillsController < ApplicationController

  autocomplete :skill, :name

  def show
    @skill = Skill.find(params[:id])
  end

end
