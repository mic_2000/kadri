class HomeController < ApplicationController
  skip_before_filter :authenticate_user!
  def index
    @resumes = Resume.active.reorder('created_at desc').last(10)
    @vacancies = Vacancy.active.reorder('created_at desc').last(10)
  end
end
