# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  skills.setupForm()

this.skills =
  setupForm: ->

    $('#skill').bind "railsAutocomplete.select", (event, data) ->
      add_skill(data.item.value);

    $('#add_skill').click (event) ->
      add_skill($('#skill').val()) unless $('#skill').val().length == 0
      false

    $('.skills .close').click ->
      $(this).parent().remove();
      skills_html = label_skills(skills_list());
      $('.skills').empty().append(skills_html);
      $('#skill').val('')
      $('.skill_list').val(skills_list().join(','));
      skills.setupForm();
      false

this.add_skill = (skill) ->
  skills_html = label_skills(skills_list(skill));
  $('.skills').empty().append(skills_html);
  $('#skill').val('')
  $('.skill_list').val(skills_list().join(','));
  skills.setupForm();
  false

this.skills_list = (skill)->
  skills = []
  $('.skills span').clone().children().remove().end().each ->
    skills.push($(this).text())
  skills.push(skill) if skill
  unique(skills)

this.label_skills = (skills) ->
  skills_html = ''
  prepend = "<span class='label label-success'><a class='close'>&times;</a>"
  for skill in skills
    skills_html += prepend + skill + '</span>'
  skills_html

this.unique = (array) ->
  array.filter (el, index, arr) ->
    index is arr.indexOf(el)
