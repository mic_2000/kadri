module SkillsHelper
  def skills_label skills, close=false
    prepend = "<span class='label label-success'>"
    prepend += "<a class='close'>&times;</a>" if close
    skill_list = skills.map do |skill|
      prepend + skill + '</span>'
    end
    raw '<div class="skills">' + skill_list.join + '</div>'
  end

  def skill_label skill
    raw "<span class='label label-success'>" + skill + '</span>'
  end
end
