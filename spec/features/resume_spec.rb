require 'spec_helper'

describe Resume do
  context "create new resume" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in(@user)
    end

    it "should add skill label", js: true do
      visit new_resume_path
      fill_in "skill", :with => "Ruby"
      click_link 'add_skill'
      page.find('.skills').find('.label').should have_content('Ruby')
    end

    it "should add skills to hiden field separeted by comma", js: true do
      visit new_resume_path
      fill_in "skill", :with => "Ruby"
      click_link 'add_skill'
      fill_in "skill", :with => "Rails"
      click_link 'add_skill'
      find('#resume_skill_list').value.should have_content('Ruby,Rails')
    end

    it "should remove skill label", js: true do
      visit new_resume_path
      fill_in "skill", :with => "Ruby"
      click_link 'add_skill'
      find('.skills').find('.label').find('a').click
      page.should_not have_content('Ruby')
    end

    it "should remove skills from hiden field when label was deleted", js: true do
      visit new_resume_path
      fill_in "skill", :with => "Ruby"
      click_link 'add_skill'
      fill_in "skill", :with => "Rails"
      click_link 'add_skill'
      find('.skills').first('.label').find('a').click
      find('#resume_skill_list').value.should have_content('Rails')
    end
  end
end


