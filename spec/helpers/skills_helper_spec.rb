require 'spec_helper'

describe SkillsHelper do
  before(:each) do
    @resume = FactoryGirl.create(:resume)
    @resume.skill_list = "Ruby"
    @resume.save
  end

  it "should html labels code with close button" do
    label_html = skills_label(@resume.skill_list, true)
    label_html.should == "<div class=\"skills\"><span class='label label-success'><a class='close'>&times;</a>Ruby</span></div>"
  end

  it "should html labels code without close button" do
    label_html = skills_label(@resume.skill_list)
    label_html.should == "<div class=\"skills\"><span class='label label-success'>Ruby</span></div>"
  end

  it "should html one label code" do
    label_html = skill_label(@resume.skill_list.first)
    label_html.should == "<span class='label label-success'>Ruby</span>"
  end
end