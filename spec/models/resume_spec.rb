require 'spec_helper'

describe Resume do
  it { should belong_to(:user) }
  it { should have_many(:skillings) }
  it { should have_many(:skills).through(:skillings) }

  context "test skills read & write:" do
    before(:each) do
      @resume1 = FactoryGirl.create(:resume)
      @resume2 = FactoryGirl.create(:resume)
    end

    it "after write skills separated by comma, should split and crete relation" do
      @resume1.skill_list = "ruby, rails"
      @resume1.skills.size.should == 2
    end

    it "after add repeating skill, skills should be uniqueness" do
      @resume1.skill_list = "ruby, rails"
      @resume1.save
      @resume2.skill_list = "rails"
      @resume2.save
      Skill.all.size.should == 2
    end
  end

  context "find relevant vacancies:" do
    before(:each) do
      @resume = FactoryGirl.create(:resume, skill_list: "ruby, rails, php")
      @vacancy1 = FactoryGirl.create(:vacancy, skill_list: "ruby, rails, php", salary: 100)
      @vacancy2 = FactoryGirl.create(:vacancy, skill_list: "ruby, rails", salary: 800)
      @vacancy3 = FactoryGirl.create(:vacancy, skill_list: "ruby, rails, php, c#", salary: 500)
      @vacancy4 = FactoryGirl.create(:vacancy, skill_list: "c#")
      @vacancy5 = FactoryGirl.create(:vacancy, skill_list: "ruby, rails, php", duedate: Date.today - 1.day)
      @vacancy6 = FactoryGirl.create(:vacancy, skill_list: "ruby, rails, php", salary: 1000)
    end

    it "should find full matched vacancy" do
      @resume.fully_matched.should == [@vacancy1, @vacancy2, @vacancy5, @vacancy6]
    end

    it "should find partially matched vacancy" do
      @resume.partially_matched.should == [@vacancy1, @vacancy2, @vacancy3, @vacancy5, @vacancy6]
    end

    it "should find full matched vacancy, only active vacancy, order by salary desc" do
      @resume.fully_matched_resumes.should == [@vacancy6, @vacancy2, @vacancy1]
    end

    it "should find partially matched vacancy, only active vacancy, order by salary desc" do
      @resume.partially_matched_resumes.should == [@vacancy6, @vacancy2, @vacancy3, @vacancy1]
    end

  end
end
