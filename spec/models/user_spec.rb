require 'spec_helper'

describe User do
  it { should have_many(:resumes) }
  it { should have_many(:vacancies) }

  context "abilities" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @admin = FactoryGirl.create(:admin)
      @resume1 = FactoryGirl.create(:resume, user: @user)
      @resume2 = FactoryGirl.create(:resume)
    end

    it "user should manage own resume" do
      Ability.new(@user).should be_able_to(:manage, @resume1)
    end

    it "user should not manage another resume" do
      Ability.new(@user).should_not be_able_to(:manage, @resume2)
    end

    it "admin should manage all resume" do
      Ability.new(@admin).should be_able_to(:manage, @resume1)
      Ability.new(@admin).should be_able_to(:manage, @resume2)
    end

  end
end
