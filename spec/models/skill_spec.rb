require 'spec_helper'

describe Skill do
  it { should have_many(:skillings) }
  it { should have_many(:resumes).through(:skillings) }
  it { should have_many(:vacancies).through(:skillings) }
end
