require 'spec_helper'

describe Vacancy do
  it { should belong_to(:user) }
  it { should have_many(:skillings) }
  it { should have_many(:skills).through(:skillings) }

  context "test skills read & write:" do
    before(:each) do
      @vacancy1 = FactoryGirl.create(:resume)
      @vacancy2 = FactoryGirl.create(:resume)
    end

    it "after write skills separated by comma, should split and crete relation" do
      @vacancy1.skill_list = "asd, qwe"
      @vacancy1.skills.size.should == 2
    end

    it "after add repeating skill, skills should be uniqueness" do
      @vacancy1.skill_list = "asd, qwe"
      @vacancy1.save
      @vacancy2.skill_list = "qwe"
      @vacancy2.save
      Skill.all.size.should == 2
    end
  end

  context "find relevant resumes:" do
    before(:each) do
      @vacancy = FactoryGirl.create(:vacancy, skill_list: "ruby, rails, php")
      @resume1 = FactoryGirl.create(:resume, skill_list: "ruby, rails, php", salary: 300)
      @resume2 = FactoryGirl.create(:resume, skill_list: "ruby, rails", salary: 200)
      @resume3 = FactoryGirl.create(:resume, skill_list: "ruby, rails, php, c#", salary: 1500)
      @resume4 = FactoryGirl.create(:resume, skill_list: "c#")
      @resume5 = FactoryGirl.create(:resume, skill_list: "ruby, rails, php", status: false)
      @resume6 = FactoryGirl.create(:resume, skill_list: "ruby, rails, php", salary: 100)
    end

    it "should find 1 full matched resume" do
      @vacancy.fully_matched.should == [@resume1, @resume3, @resume5, @resume6]
    end

    it "should find 3 partially matched resume" do
      @vacancy.partially_matched.should == [@resume1, @resume2, @resume3, @resume5, @resume6]
    end

    it "should find full matched resume, only active resume, order by salary asc" do
      @vacancy.fully_matched_vacancies.should == [@resume6, @resume1, @resume3]
    end

    it "should find partially matched resume, only active resume, order by salary asc" do
      @vacancy.partially_matched_vacancies.should == [@resume6, @resume2, @resume1, @resume3]
    end
  end
end
