# encoding: UTF-8
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :resume do
    name "Иванов Иван Иванович"
    status true
    salary 1000
    phone "(123) 456-78-90"
    email { Faker::Internet.email }
  end
end
