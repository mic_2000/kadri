# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vacancy do
    name "Cool job"
    duedate Date.today + 1.month
    salary 1000
    phone "(123) 456-78-90"
    email { Faker::Internet.email }
  end
end
