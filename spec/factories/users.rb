# encoding: UTF-8

FactoryGirl.define do
  factory :user do
    name "Иванов Иван Иванович"
    email { Faker::Internet.email }
    confirmed_at Time.now
    password "111111"
  end

  factory :admin, :parent => :user do
    admin true
  end
end
