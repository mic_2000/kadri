require 'spec_helper'

describe Kadri::Application, 'configuration' do
  let(:config) { described_class.config }

  [:password].each do |param|
    it "filters #{param.inspect} from logs" do
      config.filter_parameters.should include(param)
    end
  end

  it "should contain default_url_options" do
    config.action_mailer.default_url_options[:host].should == 'localhost:3000'
  end

  it "should locale :ru" do
    config.i18n.default_locale.should == :ru
  end

end