require "spec_helper"

describe SkillsController do
  describe "routing" do

    it "routes to #show" do
      get("/skills/1").should route_to("skills#show", :id => "1")
    end

    it "routes to #autocomplete_skill_name" do
      get("/skills/autocomplete_skill_name").should route_to("skills#autocomplete_skill_name")
    end

  end
end
