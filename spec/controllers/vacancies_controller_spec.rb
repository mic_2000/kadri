require 'spec_helper'

describe VacanciesController do

  let(:valid_attributes) { { name: "Cool job", duedate: Time.now + 1.month, salary: 1000, phone: "(123) 456-78-90", email: "email@email.com" } }

  describe "GET index" do
    it "assigns all vacancies as @vacancies" do
      vacancy = FactoryGirl.create(:vacancy)
      get :index, {}
      assigns(:vacancies).should eq([vacancy])
    end
  end

  describe "GET show" do
    it "assigns the requested vacancy as @vacancy" do
      vacancy = FactoryGirl.create(:vacancy)
      get :show, {:id => vacancy.to_param}
      assigns(:vacancy).should eq(vacancy)
    end
  end

  describe "GET new" do
    it "assigns a new vacancy as @vacancy" do
      @user = FactoryGirl.create(:user)
      sign_in @user
      get :new, {}
      assigns(:vacancy).should be_a_new(Vacancy)
    end
  end

  describe "GET edit" do
    it "assigns the requested vacancy as @vacancy" do
      @user = FactoryGirl.create(:user)
      sign_in @user
      vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
      get :edit, {:id => vacancy.to_param}
      assigns(:vacancy).should eq(vacancy)
    end
  end

  describe "POST create" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end
    describe "with valid params" do
      it "creates a new Vacancy" do
        expect {
          post :create, {:vacancy => valid_attributes}
        }.to change(Vacancy, :count).by(1)
      end

      it "assigns a newly created vacancy as @vacancy" do
        post :create, {:vacancy => valid_attributes}
        assigns(:vacancy).should be_a(Vacancy)
        assigns(:vacancy).should be_persisted
      end

      it "redirects to the created vacancy" do
        post :create, {:vacancy => valid_attributes}
        response.should redirect_to(Vacancy.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved vacancy as @vacancy" do
        Vacancy.any_instance.stub(:save).and_return(false)
        post :create, {:vacancy => { "email" => "invalid email" }}
        assigns(:vacancy).should be_a_new(Vacancy)
      end

      it "re-renders the 'new' template" do
        Vacancy.any_instance.stub(:save).and_return(false)
        post :create, {:vacancy => { "email" => "invalid email" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end
    describe "with valid params" do
      it "updates the requested vacancy" do
        vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
        Vacancy.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => vacancy.to_param, :vacancy => { "name" => "MyString" }}
      end

      it "assigns the requested vacancy as @vacancy" do
        vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
        put :update, {:id => vacancy.to_param, :vacancy => valid_attributes}
        assigns(:vacancy).should eq(vacancy)
      end

      it "redirects to the vacancy" do
        vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
        put :update, {:id => vacancy.to_param, :vacancy => valid_attributes}
        response.should redirect_to(vacancy)
      end
    end

    describe "with invalid params" do
      it "assigns the vacancy as @vacancy" do
        vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
        Vacancy.any_instance.stub(:save).and_return(false)
        put :update, {:id => vacancy.to_param, :vacancy => { "name" => "invalid value" }}
        assigns(:vacancy).should eq(vacancy)
      end

      it "re-renders the 'edit' template" do
        vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
        Vacancy.any_instance.stub(:save).and_return(false)
        put :update, {:id => vacancy.to_param, :vacancy => { "name" => "invalid value" }}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end
    it "destroys the requested vacancy" do
      vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
      expect {
        delete :destroy, {:id => vacancy.to_param}
      }.to change(Vacancy, :count).by(-1)
    end

    it "redirects to the vacancies list" do
      vacancy = FactoryGirl.create(:vacancy, user_id: @user.id)
      delete :destroy, {:id => vacancy.to_param}
      response.should redirect_to(vacancies_url)
    end
  end

end
