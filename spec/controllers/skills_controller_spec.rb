require 'spec_helper'

describe SkillsController do
  describe "GET show" do
    it "assigns the requested skill as @skill" do
      skill = FactoryGirl.create(:skill)
      get :show, {:id => skill.to_param}
      assigns(:skill).should eq(skill)
    end
  end
end
