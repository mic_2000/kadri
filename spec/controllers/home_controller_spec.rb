require 'spec_helper'

describe HomeController do

  describe "GET index" do
    it "should show 10 latest resumes and vacancies" do
      resumes = 20.times.map {|i| FactoryGirl.create(:resume)}
      vacancies = 20.times.map {|i| FactoryGirl.create(:vacancy)}
      resume2 = FactoryGirl.create(:resume)
      get :index, {}
      assigns(:resumes).count.should eq(10)
      assigns(:vacancies).count.should eq(10)
    end
  end

end
