# encoding: UTF-8
require 'spec_helper'

describe ResumesController do
  let(:valid_attributes) { { name: "Иванов Иван Иванович", status: 1, salary: 1000, phone: "(123) 456-78-90", email: "email@email.com" } }

  describe "GET index" do
    it "assigns all resumes as @resumes" do
      resume = FactoryGirl.create(:resume)
      get :index, {}
      assigns(:resumes).should eq([resume])
    end
  end

  describe "GET show" do
    it "assigns the requested resume as @resume" do
      resume = FactoryGirl.create(:resume)
      get :show, {:id => resume.to_param}
      assigns(:resume).should eq(resume)
    end
  end

  describe "GET new" do
    it "assigns a new resume as @resume" do
      @user = FactoryGirl.create(:user)
      sign_in @user
      get :new, {}
      assigns(:resume).should be_a_new(Resume)
    end
  end

  describe "GET edit" do
    it "assigns the requested resume as @resume" do
      @user = FactoryGirl.create(:user)
      sign_in @user
      resume = FactoryGirl.create(:resume, user_id: @user.id)
      get :edit, {:id => resume.to_param}
      assigns(:resume).should eq(resume)
    end
  end

  describe "POST create" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end

    describe "with valid params" do
      it "creates a new Resume" do
        expect {
          post :create, {:resume => valid_attributes}
        }.to change(Resume, :count).by(1)
      end

      it "assigns a newly created resume as @resume" do
        post :create, {:resume => valid_attributes}
        assigns(:resume).should be_a(Resume)
        assigns(:resume).should be_persisted
      end

      it "redirects to the created resume" do
        post :create, {:resume => valid_attributes}
        response.should redirect_to(Resume.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved resume as @resume" do
        Resume.any_instance.stub(:save).and_return(false)
        post :create, {:resume => { "name" => "invalid value" }}
        assigns(:resume).should be_a_new(Resume)
      end

      it "re-renders the 'new' template" do
        Resume.any_instance.stub(:save).and_return(false)
        post :create, {:resume => { "name" => "invalid value" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end
    describe "with valid params" do
      it "updates the requested resume" do
        resume = FactoryGirl.create(:resume, user_id: @user.id)
        Resume.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => resume.to_param, :resume => { "name" => "MyString" }}
      end

      it "assigns the requested resume as @resume" do
        resume = FactoryGirl.create(:resume, user_id: @user.id)
        put :update, {:id => resume.to_param, :resume => valid_attributes}
        assigns(:resume).should eq(resume)
      end

      it "redirects to the resume" do
        resume = FactoryGirl.create(:resume, user_id: @user.id)
        put :update, {:id => resume.to_param, :resume => valid_attributes}
        response.should redirect_to(resume)
      end
    end

    describe "with invalid params" do
      it "assigns the resume as @resume" do
        resume = FactoryGirl.create(:resume, user_id: @user.id)
        Resume.any_instance.stub(:save).and_return(false)
        put :update, {:id => resume.to_param, :resume => { "name" => "invalid value" }}
        assigns(:resume).should eq(resume)
      end

      it "re-renders the 'edit' template" do
        resume = FactoryGirl.create(:resume, user_id: @user.id)
        Resume.any_instance.stub(:save).and_return(false)
        put :update, {:id => resume.to_param, :resume => { "name" => "invalid value" }}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in @user
    end

    it "destroys the requested resume" do
      resume = FactoryGirl.create(:resume, user_id: @user.id)
      expect {
        delete :destroy, {:id => resume.to_param}
      }.to change(Resume, :count).by(-1)
    end

    it "redirects to the resumes list" do
      resume = FactoryGirl.create(:resume, user_id: @user.id)
      delete :destroy, {:id => resume.to_param}
      response.should redirect_to(resumes_url)
    end
  end

end
