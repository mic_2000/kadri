Kadri::Application.routes.draw do
  resources :skills, only: 'show' do
    get :autocomplete_skill_name, :on => :collection
  end

  resources :resumes
  resources :vacancies

  devise_for :users
  root :to => "home#index"

end
