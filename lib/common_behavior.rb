module CommonBehavior
  def self.included(base)
    base.module_eval do
      has_many :skillings, :as => :skillable
      has_many :skills, :through => :skillings

      belongs_to :user

      validates :salary, numericality: { only_integer: true }, :unless => "salary.blank?"
      validates :phone, format: /\A([\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}\z/, if: 'phone.present?'
      validates_format_of :email, :with => Devise.email_regexp

      before_save :fix_skills
      after_destroy :destroy_relation
    end
  end

  def skill_list
    self.skills.map(&:name)
  end

  def skill_list= (skill_list)
    self.skills.clear
    skill_list.split(',').each do |skill|
      self.skills.build(:name => skill.strip)
    end
  end

  def partially_matched
    resource.where(id: find_ids)
  end

  def fully_matched
    matched_objects_ids = []
    objects = resource.where(id: find_ids).includes(:skills)
    objects.each do |object|
      matched_objects_ids << object.id if check_skills?(self, object)
    end
    resource.where(id: matched_objects_ids)
  end

private
  def check_skills? object1, object2
    vacancy = object1.class == Vacancy ? object1 : object2
    resume = object1.class == Resume ? object1 : object2
    vacancy.skills.all?{ |skill| resume.skills.all.include?(skill) }
  end

  def find_ids
    Skilling.where(skill_id: self.skills.map(&:id), skillable_type: resource.name).map(&:skillable_id).uniq
  end

  def resource
    self.class == Resume ? Vacancy : Resume
  end

  def fix_skills
    if self.skills.loaded?
      new_skills = []
      self.skills.each do |skill|
        if existing = Skill.find_by_name(skill.name)
          new_skills << existing
        else
          new_skills << skill
        end
      end
      self.skills = new_skills.uniq
    end
  end

  def destroy_relation
    Skilling.where(skillable_type: self.class.name).where(skillable_id: self.id).delete_all
  end
end
