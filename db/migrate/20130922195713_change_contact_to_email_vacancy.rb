class ChangeContactToEmailVacancy < ActiveRecord::Migration
  def up
    rename_column :vacancies, :contact, :email
    add_column :vacancies, :phone, :string
  end

  def down
    rename_column :vacancies, :email, :contact
    remove_column :vacancies, :phone
  end
end
