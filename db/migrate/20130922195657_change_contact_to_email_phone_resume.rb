class ChangeContactToEmailPhoneResume < ActiveRecord::Migration
  def up
    rename_column :resumes, :contact, :email
    add_column :resumes, :phone, :string
  end

  def down
    rename_column :resumes, :email, :contact
    remove_column :resumes, :phone
  end
end
