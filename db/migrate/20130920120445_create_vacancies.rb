class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.date :duedate
      t.integer :salary
      t.string :contact
      t.integer :user_id

      t.timestamps
    end
  end
end
