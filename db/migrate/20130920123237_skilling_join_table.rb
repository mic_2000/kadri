class SkillingJoinTable < ActiveRecord::Migration
  def change
    create_table :skillings do |t|
      t.integer :skill_id
      t.references :skillable, polymorphic: true
      t.timestamps
    end
  end
end
