class CreateResumes < ActiveRecord::Migration
  def change
    create_table :resumes do |t|
      t.string :name
      t.boolean :status
      t.integer :salary
      t.string :contact
      t.integer :user_id

      t.timestamps
    end
  end
end
