
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# User.create({email: 'admin@admin.com', password: '123456', admin:true}, :without_protection => true)


require "#{Rails.root}/db/seeds/resumes.rb"
require "#{Rails.root}/db/seeds/skillings.rb"
require "#{Rails.root}/db/seeds/skills.rb"
require "#{Rails.root}/db/seeds/users.rb"
require "#{Rails.root}/db/seeds/vacancies.rb"