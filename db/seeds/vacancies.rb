# encoding: UTF-8

Vacancy.create({:id => 2, :name => "Инженер IT, системный администратор", :duedate => Date.today + 1.month, :salary => 1500, :email => "keanu.kuhlman@huel.org", :user_id => 2, :phone => "(045) 235-15-76"}, :without_protection => true)
Vacancy.create({:id => 1, :name => "Операционный директор", :duedate => Date.today + 1.month, :salary => 2000, :email => "nigel_roob@oreillyturner.name", :user_id => 2, :phone => "(045) 894-15-65"}, :without_protection => true)
Vacancy.create({:id => 3, :name => "Дизайнер", :duedate => Date.today + 1.month, :salary => 800, :email => "porter_schoen@predovic.biz", :user_id => 2, :phone => "(038) 758-15-75"}, :without_protection => true)
Vacancy.create({:id => 4, :name => "Оператор контактного центра", :duedate => Date.today + 1.month, :salary => 400, :email => "raymond_nader@bartoletti.info", :user_id => 2, :phone => "(038) 345-46-75"}, :without_protection => true)
Vacancy.create({:id => 5, :name => "Оператор колл-центра банка", :duedate => Date.today - 1.month, :salary => 1000, :email => "maxime@waelchi.biz", :user_id => 2, :phone => "(038) 497-46-75"}, :without_protection => true)
Vacancy.create({:id => 6, :name => "Фитнес-специалист", :duedate => Date.today + 1.month, :salary => 900, :email => "pattie_heidenreich@lockmanfarrell.info", :user_id => 3, :phone => "(045) 256-15-65"}, :without_protection => true)
Vacancy.create({:id => 7, :name => "Менеджер по логистике", :duedate => Date.today + 1.month, :salary => 1700, :email => "bernadette_glover@hilpert.name", :user_id => 3, :phone => "(045) 496-15-73"}, :without_protection => true)
