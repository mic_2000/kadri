# encoding: UTF-8

Resume.create({:id => 1, :name => "Иванов Дмитрий Владимирович", :status => true, :salary => 500, :email => "gene@kub.info", :user_id => 2, :phone => "(123) 123-45-67"}, :without_protection => true)
Resume.create({:id => 3, :name => "Петров Олег Анатольевич", :status => true, :salary => 2000, :email => "anabel@halvorsonhane.net", :user_id => 2, :phone => "(380) 468-15-68"}, :without_protection => true)
Resume.create({:id => 4, :name => "Смиронов Евгений Иванович", :status => true, :salary => 1700, :email => "freeman@konopelski.info", :user_id => 2, :phone => "(380) 358-54-56"}, :without_protection => true)
Resume.create({:id => 5, :name => "Федоров Алексей Николаевич", :status => false, :salary => 900, :email => "pat@spinkawuckert.biz", :user_id => 2, :phone => "(380) 873-87-56"}, :without_protection => true)
Resume.create({:id => 2, :name => "Семенов Александр Александрович", :status => false, :salary => 800, :email => "dedric@wolf.biz", :user_id => 2, :phone => "(545) 132-46-45"}, :without_protection => true)
Resume.create({:id => 6, :name => "Виноградов  Валентин Анатольевич", :status => true, :salary => 1000, :email => "ned_hartmann@kreiger.net", :user_id => 3, :phone => "(545) 487-456-45"}, :without_protection => true)
Resume.create({:id => 7, :name => "Сидор Евгений Иванович", :status => true, :salary => 1500, :email => "tevin.cole@toy.info", :user_id => 3, :phone => "(380) 378-15-68"}, :without_protection => true)
